const { assert, expect } = require("chai")
const { newUser } = require("../index.js")

// assert that the new User first name type is string
// assert new user last name type is string
// assert new user first name is not undefined
// assert new user last name is not undefined
// assert new User age is at least 18 
// assert newUser age is type number
// assert newUser contact number type is string
// assert newUser batch number type is number
// assert new user batch number is 151

describe('Assert newUser is of the right data structure', () => {
	it('that firstName is of type string', () => {
		assert.typeOf(newUser.firstName, 'string')
	})

	it('that lastName is of type string', () => {
		assert.typeOf(newUser.lastName, 'string')
	})

	it('that firstName is not undefined', () => {
		assert.notTypeOf(newUser.firstName, 'undefined')
	})

	it('that lastName is not undefined', () => {
		assert.notTypeOf(newUser.lastName, 'undefined')
	})
	it('that age is at least 18', () => {
		assert.isAtLeast(newUser.age, 18);
	})

	it('athat ge is of type number', () => {
		assert.typeOf(newUser.age, 'number')
	})

	it('that contactNumber is of type string', () => {
		assert.typeOf(newUser.contactNumber, 'string')
	})

	it('that batchNumber if of type number', () => {
		assert.typeOf(newUser.batchNumber, 'number')
	})

	it('that batchNumber is 151', () => {
		assert.equal(newUser.batchNumber, 151)
	})
});