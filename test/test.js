const { assert, expect } = require("chai")
const { newUser } = require("../index.js")

// describe gives structure to your test suite
describe('Test newUser object', () => {
	it('Assert newUser type is object', () => {
		// assert.equal(typeof(newUser), 'object');
		expect(newUser).to.be.an('object')
	});

	it('Assert newUser.email is type string', () => {
		// assert.equal(typeof(newUser.email), 'string');
		// assert.typeOf(newUser.email, 'string')
		expect(newUser.email).to.be.a('string')
	})

	it('Assert email is not undefined', () => {
		// assert.notEqual(typeof(newUser.email), 'undefined');
		// assert.notEqual
		// assert.notTypeOf
		expect(newUser.email).to.not.be.an('undefined')
	})

	it('Assert that newUser.password is type string', () => {
		assert.typeOf(newUser.password, 'string');
	})

	it('Assert that passsword length is 16 characters long', () => {
		assert.isAtLeast(newUser.password.length, 16);
	})
})