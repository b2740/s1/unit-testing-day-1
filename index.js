// Mini-Activity:
// Instructions:
// 		1. Construct a basic express app
//		2. Have a port running in port 5000
//		3. Make sure to console.log that your port is running

const express = require("express");

const app = express();
const port = 5000;


app.listen(port, () => {
	console.log(`Server is now running at port ${port}`);
})

const newUser = {
	firstName: 'John',
	lastName: 'Dela Cruz',
	age: 18,
	contactNumber: '09123456789',
	batchNumber: 151,
	email: "john.delacruz@gmail.com",
	password: "iamjohnwithsixteencharactersinlength"
}

module.exports = {
	newUser: newUser
}